const jwt = require("../../MobileBasedLogin/utils/jwt.js");
const { createJsonResponse } = require("../../MobileBasedLogin/utils/serverResponse.js");

module.exports = (req, res, next) => {
    // check token if available
    const token = req.headers.authorization && req.headers.authorization !== "";

    if (!token) {
        return res.status(400).json(createJsonResponse(400, "No authorization token"));
    }

    // check payload
    try {
        const payload = jwt.verifyJWT(req.headers.authorization.split(" ")[1]);
        req.payload = payload;

        next();
    } catch (error) {
        if (error.name === "TokenExpiredError") {
            return res.status(400).json(createJsonResponse(400, "Authorization token expire"));
        }

        return res.status(400).json(createJsonResponse(400, "No authorization token"));
    }
};

// Middleware to verify if app  user is a valid user
module.exports = async (req, res, next) => {
    try {
        // Get json-web-token
        let jwt = req.signedCookies[cookieProps.key] || (req.query && req.query['access_token']) || req.headers['x-access-token'] || req.headers['authorization'];
        if (!jwt) {
            throw Error('JWT not present.');
        }
        if (jwt.startsWith('Bearer ')) {
            // Remove Bearer from string
            jwt = jwt.slice(7, jwt.length);
        }
       
        const clientData = await jwtService.decodeJwt(jwt);
        console.log("client data", clientData);
       
        res.locals.userId = clientData.id;
        next();
    } catch (err) {
        return res.status(UNAUTHORIZED).json({
            error: err.message,
        });
    }
};



