exports.createJsonResponse = (suc = "", code, msg = "", data = {}) => {
  if (suc == 400) {
    return { statusCode: suc, message: code };
  }

  if (Array.isArray(data)) {
    // return {data, count: data.length , message: msg,success: suc, response_code: code,  };
    return { statusCode: code, message: msg, data };
  }

  // return { data,message: msg, success:suc,response_code: code,  };
  return { statusCode: code, message: msg, data };
};
