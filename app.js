const express = require("express");
const os = require("os");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const formData = require("express-form-data");
const path = require("path");
const routes = require("./src/routes/router.js");
const fileupload = require("express-fileupload");

const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

const { createJsonResponse } = require("./utils/serverResponse.js");
const User = require("./src/models/User.js");
const jwt = require("./utils/jwt.js");

// const fs = require("fs");

const app = express();
app.use(fileupload());

app.post(
  "/saloon/image-upload",
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    // console.log("here");
    // console.log("File", req.files);
    const usertoken = req.headers.authorization;
    // console.log("UserPassword::", usertoken);
    if (usertoken) {
      const token = usertoken.split(" ");
      if (token) {
        const decoded = jwt.verifyJWT(token[1], "secret-key");
        const userId = decoded.id;
        // console.log("USerId", userId);
        if (req.files) {
          // console.log(req.files);
          const file = req.files.file;
          const fileName = file.name;
          // console.log("FileName", fileName);

          file.mv("./public/" + fileName, (err) => {
            if (err) {
              res.send(err);
            } else {
              console.log("File Uploaded");
            }
          });

          const [fileupload] = await User.updateUserImage({ userId, fileName });
          // console.log("dshfsd", fileupload);
          const [userRows] = await User.getUserById(userId);
          // console.log("UserRow", userRows);
          const userData = fileName;
          
          // delete userData.deviceToken;
          // delete userData.loginType;
          // delete userData.soicalId;

          return res
            .status(200)
            .json(createJsonResponse("OK", 200, "File Uploaded", userData ));
        } else {
          return res
            .status(400)
            .json(createJsonResponse(400, "File Not Uploaded"));
        }
      } else {
        return res.status(400).json(createJsonResponse(400, "No Token Found"));
      }
    } else {
      return res
        .status(400)
        .json(createJsonResponse(400, "No Authentication Token Found"));
    }
  })
);

const options = {
  uploadDir: os.tmpdir(),
  autoClean: true,
};

app.use(formData.parse(options));
app.use(cors());
app.use(express.json());
// app.use(forms.array());
app.use(express.urlencoded({ extended: true })); // support encoded bodies

app.use(cookieParser());

// app.use(bodyParser.json()); // support json encoded bodies
// // app.use(forms.array());
// app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies

app.use(express.static("public"));

app.use("/", routes);

module.exports = app;
