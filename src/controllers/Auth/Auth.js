const express = require("express");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");
const jwt = require("../../../utils/jwt.js");

const utils = require("../../../utils/utils.js");
const { createJsonResponse } = require("../../../utils/serverResponse.js");

require("dotenv").config();

const smsKey = process.env.SMS_SECRET_KEY;
const mailKey = process.env.EMAIL_SECRET_KEY;
const twilioNum = process.env.TWILIO_PHONE_NUMBER;
const crypto = require("crypto");

const nodemailer = require("nodemailer");

const accountSid = process.env.ACCOUNT_SID;
const authToken = process.env.AUTH_TOKEN;
const client = require("twilio")(accountSid, authToken);
const JWT_AUTH_TOKEN = process.env.JWT_AUTH_TOKEN;

const User = require("../../models/User.js");

const authRouter = express.Router();

/**
 * Logout
 */

authRouter.get(
  "/logout",
  asyncHandler(async (req, res) => {
    const usertoken = req.headers.authorization;

    // console.log(usertoken)

    if (usertoken == undefined) {
      return res
        .status(200)
        .json(
          createJsonResponse(400, "Session expired, please login again.", {})
        );
    }
    return res
      .status(200)
      .json(createJsonResponse("OK", 200, "Logout successfully.", {}));
  })
);

/********************************************************
 * ************************** Register User
 *********************************************************/

authRouter.post(
  "/signUp",
  [
    body("nameData")
      .exists()
      .withMessage("name must be exist")
      .bail()
      .notEmpty()
      .withMessage("The name field is required")
      .bail(),
    body("email")
      .exists()
      .withMessage("email must be exist")
      .bail()
      .notEmpty()
      .withMessage("The email field is required")
      .bail()
      .isEmail()
      .withMessage("email should be a valid type")
      .bail(),
    body("countryCode")
      .exists()
      .withMessage("countryCode must be exist")
      .bail()
      .notEmpty()
      .withMessage("The countryCode field is required")
      .bail(),
    body("contactNumber")
      .exists()
      .withMessage("contactNumber must be exist")
      .bail()
      .notEmpty()
      .withMessage("The contactNumber field is required")
      .bail(),
    body("gender")
      .exists()
      .withMessage("gender must be exist")
      .bail()
      .notEmpty()
      .withMessage("The gender field is required")
      .bail(),
    body("password")
      .exists()
      .withMessage("password must be exist")
      .bail()
      .notEmpty()
      .withMessage("The password field is required")
      .bail(),
    body("deviceType")
      .exists()
      .withMessage("deviceType must be exist")
      .bail()
      .notEmpty()
      .withMessage("The deviceType field is required")
      .bail(),
    
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg, {}));
    }

    const { email, password, contactNumber } = req.body;

    const [EmailRow] = await User.getUserByEmail(email);

    // console.log("EmailRows", EmailRow);

    if (EmailRow.length > 0) {
      return res
        .status(400)
        .json(
          createJsonResponse(
            400,
            "Your email address is already registered, Please try to login!",
            {}
          )
        );
    }

    const [MobileRow] = await User.getUserByMobile(contactNumber);

    if (MobileRow.length > 0) {
      return res
        .status(400)
        .json(
          createJsonResponse(
            400,
            "The mobile number has already been taken.",
            {}
          )
        );
    }

    const otp = Math.floor(100000 + Math.random() * 900000);
    const ttl = 2 * 60 * 1000;
    const expires = Date.now() + ttl;
    const data = `${contactNumber}.${otp}.${expires}`;
    const hash = crypto.createHmac("sha256", smsKey).update(data).digest("hex");
    const tokenData = `${hash}.${expires}`;

    await User.insertUser({
      ...req.body,
      password: utils.encryptPassword(password),
    });

    const [userRows] = await User.getUserByMobile(contactNumber);

    // console.log("USErRow",userRows)

    // console.log("UserRows", userRows.insertId);

    const token = jwt.generateJWT({ id: userRows[0].id });

    await User.updateUserToken({ token, id: userRows[0].id });

    const [newUser] = await User.getUserByEmail(email);

    // console.log("sdgffsdbg", newUser);

    delete req.body.password;

    client.messages
      .create({
        body: `Your One Time Login Password For CFM is ${otp}`,
        from: +17372068970,
        to: contactNumber,
      })
      .then((messages) => console.log(messages))
      .catch((err) => console.error(err));

    // res.status(200).send({ email, hash: fullHash, otp }); // this bypass otp via api only for development instead hitting twilio api all the time

    // const hashData = res.status(200).send({ email, token: fullHash, otp });
    const hashData = { email, tokenData: tokenData, otp };

    let userData = req.body;

    sendMail(userData);

    /**
     *
     * SEnding email to user
     *  */

    function sendMail(userData) {
      // create reusable transporter object using the default SMTP transport
      // console.log("djshfgsdhfjh", user);
      let mailTransporter = nodemailer.createTransport({
        service: "gmail",
        host: "smtp.ethereal.email",
        port: 587,
        secure: false,
        auth: {
          user: "tku22658@gmail.com",
          pass: "tinkuDemo@1234?",
        },
      });

      let mailDetails = {
        from: "tku22658@gmail.com", //-------- sender email
        to: userData.email, //-------- receiver email
        subject: "Test mail", //-------- subject do you want
        text: "Node.js testing mail for SIT", //-------- message
        html: `<b>Hello ${userData.full_name}?</b><br><a>Your Varification code is ${otp}</a>`, // html body
      };

      mailTransporter.sendMail(mailDetails, function (err, data) {
        if (err) {
          console.log("Error Occurs");
        } else {
          console.log("Email sent successfully");
        }
      });
    }

    const user = newUser[0];
    // console.log("UserData",userData)

    delete user.password;
    delete user.soicalId;
    delete user.loginType;
    delete user.deviceToken;
    // delete user.token;

    return res
      .status(200)
      .json(
        createJsonResponse(
          "OK",
          200,
          "Signup Successfully. Otp send on your mobile.",
          { ...user }
        )
      );
  })
);

/**
 * @route POST "/api/auth/login"
 */
authRouter.post(
  "/login",
  [
    body("countryCode")
      .exists()
      .withMessage("countryCode must be exist")
      .bail()
      .notEmpty()
      .withMessage("The countryCode field is required")
      .bail(),
    body("contactNumber")
      .exists()
      .withMessage("contactNumber must be exist")
      .bail()
      .notEmpty()
      .withMessage("The contactNumbers field is required")
      .bail(),
    body("password")
      .exists()
      .withMessage("password must be exist")
      .bail()
      .notEmpty()
      .withMessage("The password field is required")
      .bail(),
    body("deviceType")
      .exists()
      .withMessage("deviceType must be exist")
      .bail()
      .notEmpty()
      .withMessage("The deviceType field is required")
      .bail(),
    body("deviceToken")
      .exists()
      .withMessage("deviceToken must be exist")
      .bail()
      .notEmpty()
      .withMessage("The deviceToken field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg, {}));
    }

    const { countryCode, contactNumber, password } = req.body;

    // check for user
    const [userRows] = await User.getUserByMobile(contactNumber);

    if (userRows.length === 0) {
      return res
        .status(400)
        .json(
          createJsonResponse(
            400,
            "Mobile number not registered yet, Please register first!"
          )
        );
    }

    if (!utils.compareHashPassword(password, userRows[0].password)) {
      return res
        .status(400)
        .json(createJsonResponse(400, "Password does not match", {}));
    }

    if (userRows[0].isVerify == 1) {
      delete userRows[0].password;

      const user = userRows[0];
      // delete user.deviceType;
      // delete user.isVerify;
      delete user.soicalId;
      delete user.loginType;
      delete user.deviceToken;

      return res
        .status(200)
        .json(createJsonResponse("OK", 200, "Login Successfully", { ...user }));
    } else {
      return res
        .status(400)
        .json(createJsonResponse(400, "Sorry, contactNumber is not varified!"));
    }
  })
);

/*************************************************
 * verifyOTP
 ********************************/

authRouter.post(
  "/verifyOtp",
  [
    body("otp")
      .exists()
      .withMessage("otp must be exist")
      .bail()
      .notEmpty()
      .withMessage("The otp field is required")
      .bail(),
    body("userId")
      .exists()
      .withMessage("userId must be exist")
      .bail()
      .notEmpty()
      .withMessage("The userId field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    const { userId, otp } = req.body;

    console.log(req.body);

    if (otp != 123456) {
      return res.status(400).json(createJsonResponse(400, "Invalid Otp"));
    }

    const [userRows] = await User.getUserById(userId);
    console.log(userRows);

    const [userRowsData] = await User.getUserByMobile(
      userRows[0].contactNumber
    );
    // console.log(userRowsData[0])

    if (userRows.length === 0) {
      return res
        .status(400)
        .json(createJsonResponse(400, "The selected userId is invalid."));
    }

    await User.UserVerify(userId);

    delete userRowsData[0].password;

    const user = userRowsData[0];

    delete user.soicalId;
    delete user.deviceToken;
    delete user.loginType;

    user.isVerify = 1;

    return res
      .status(200)
      .json(
        createJsonResponse("OK", 200, "Otp Validate Successfully", { ...user })
      );
  })
);

/*********
 * **** Resend OTP
 *****************/
authRouter.post(
  "/resend-otp",
  [
    body("userId")
      .exists()
      .withMessage("userId must be exist")
      .bail()
      .notEmpty()
      .withMessage("The userId field is required.")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    const { userId } = req.body;

    // check for user
    const [userRows] = await User.getUserById(userId);

    if (userRows[0].length === 0) {
      return res
        .status(400)
        .json(createJsonResponse(400, "The selected userId is invalid"));
    }
    const contactNumber = userRows[0][0].contactNumber;

    const otp = Math.floor(100000 + Math.random() * 900000);
    const ttl = 2 * 60 * 1000;
    const expires = Date.now() + ttl;
    const data = `${contactNumber}.${otp}.${expires}`;
    const hash = crypto.createHmac("sha256", smsKey).update(data).digest("hex");
    const fullHash = `${hash}.${expires}`;

    client.messages
      .create({
        body: `Your One Time Login Password For CFM is ${otp}`,
        from: +17372068970,
        to: contactNumber,
      })
      .then((messages) => console.log(messages))
      .catch((err) => console.error(err));

    const hashData = { contactNumber, hash: fullHash, otp };

    return res
      .status(200)
      .json(createJsonResponse("OK", 200, "Otp has been resend successfully."));
  })
);

/**
 * @route PUT "/api/User/updatePassword"
 */
authRouter.put(
  "/changePassword",
  [
    body("current_password")
      .exists()
      .withMessage("current_password must be exist")
      .bail()
      .notEmpty()
      .withMessage("current_password should not be empty")
      .bail(),
    body("new_password")
      .exists()
      .withMessage("new_password must be exist")
      .bail()
      .notEmpty()
      .withMessage("new_password should not be empty")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    // console.log("Whole Body::", req.body);

    const usertoken = req.headers.authorization;
    // console.log("UserPassword::", usertoken);

    const token = usertoken.split(" ");

    // console.log("Token::", token);

    if (token) {
      const decoded = jwt.verifyJWT(token[1], "secret-key");
      // console.log(decoded.id);
      // jwt.verifyJWT(token[1], "secret-key");
      const userId = decoded.id;

      // console.log("Whole Body1::", req.body);

      const takePassword = await User.takePasswordById(userId);

      // console.log("TakePassword::", takePassword[0][0].password);
      const { new_password } = req.body;
      const password = new_password;
      // console.log("NewPassword::", new_password);
      const { current_password } = req.body;
      const currentPassword = utils.encryptPassword(current_password);
      // console.log("CurrentPassword::", currentPassword);

      // const deTakePassword = utils.encryptPassword(123456);
      // console.log("DecodePassword::", deTakePassword)

      // if (currentPassword === takePassword[0][0].password) {
      await User.updateUserPassword({
        password: utils.encryptPassword(new_password),
      });
      return res
        .status(200)
        .json(
          createJsonResponse("OK", 200, "Password has been reset successfully")
        );
    }
  })
);

/**
 * ResetPassword
 */

authRouter.post(
  "/reset-password",
  [
    body("userId")
      .exists()
      .withMessage("userId must be exist")
      .bail()
      .notEmpty()
      .withMessage("The userId field is required")
      .bail(),
    body("password")
      .exists()
      .withMessage("password must be exist")
      .bail()
      .notEmpty()
      .withMessage("The password field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }
    const { userId, password } = req.body;

    const [userRows] = await User.getUserById(userId);

    if (userRows[0].length === 0) {
      return res
        .status(400)
        .json(createJsonResponse(400, "The selected userId is invalid."));
    }
    const [UserRow] = await User.resetPassword({
      password: utils.encryptPassword(password),
      userId,
    });

    return res.json(
      createJsonResponse("OK", 200, "Your password changed successfully")
    );
  })
);

/**
 * Forgot Password
 */
authRouter.post(
  "/forgot-password",
  [
    body("countryCode")
      .exists()
      .withMessage("countryCode must be exist")
      .bail()
      .notEmpty()
      .withMessage("The countryCode field is required")
      .bail(),
    body("contactNumber")
      .exists()
      .withMessage("contactNumber must be exist")
      .bail()
      .notEmpty()
      .withMessage("The contactNumber field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    const { countryCode, contactNumber } = req.body;

    // check for user
    const [userRows] = await User.getUserByMobile(contactNumber);
    // console.log("USerRows", userRows[0]);

    if (userRows.length === 0) {
      return res
        .status(400)
        .json(
          createJsonResponse(400, "The selected contactNumber is invalid.")
        );
    }

    // console.log(userRows[0]);

    const contactNumberData = "+91 " + contactNumber;

    // console.log("Cidshgsdfhj", contactNumberData);

    const phone = countryCode + contactNumber;
    const otp = Math.floor(100000 + Math.random() * 900000);
    const ttl = 2 * 60 * 1000;
    const expires = Date.now() + ttl;
    const data = `${phone}.${otp}.${expires}`;
    const hash = crypto.createHmac("sha256", smsKey).update(data).digest("hex");
    const fullHash = `${hash}.${expires}`;

    // console.log(phone);

    client.messages
      .create({
        body: `Your One Time Login Password For CFM is ${otp}`,
        from: +17372068970,
        to: +917986341518,
      })
      .then((messages) => console.log(messages))
      .catch((err) => console.error(err));

    const hashData = { contactNumber, token: fullHash };

    const user = userRows[0];

    delete user.soicalId;
    delete user.deviceToken;
    delete user.loginType;
    delete user.password;

    return res.status(200).json(
      createJsonResponse("Ok", 200, "Otp has been sent on registed number", {
        ...user,
      })
    );
  })
);

/******************************************************************************
 *                      Change Password - "POST /api/auth/changePassword"
 ******************************************************************************/

// authRouter.post(
//   "/changePassword",
//   [
//     body("current_password")
//       .exists()
//       .withMessage("current_password must be exist")
//       .bail()
//       .notEmpty()
//       .withMessage("The current_password field is required")
//       .bail(),
//     body("new_password")
//       .exists()
//       .withMessage("new_password must be exist")
//       .bail()
//       .notEmpty()
//       .withMessage("The new_password field is required")
//       .bail(),
//   ],
//   asyncHandler(async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res
//         .status(200)
//         .json(createJsonResponse({}, errors.array()[0].msg, false, 200));
//     }
//     const email = req.body.email;
//     const hash = req.body.hash;
//     const otp = req.body.otp;
//     const password = req.body.password;
//     const [hashValue, expires] = hash.split(".");

//     const now = Date.now();
//     if (now > parseInt(expires)) {
//       return res.status(504).send({ msg: "Timeout. Please try again" });
//     }
//     // console.log("user confirmed");
//     // console.log("phone", phone);
//     // console.log("Password:::", password);
//     User.forgotPassword(phone, password);
//     const data = `${phone}.${otp}.${expires}`;
//     const newCalculatedHash = crypto
//       .createHmac("sha256", smsKey)
//       .update(data)
//       .digest("hex");
//     if (newCalculatedHash === hashValue) {
//       User.forgotPassword({ phone, password: utils.encryptPassword(password) });

//       // const accessToken = jwt.sign({ data: phone }, JWT_AUTH_TOKEN, { expiresIn: '30s' });
//       const accessToken = jwt.generateJWT({ data: phone }, JWT_AUTH_TOKEN, {
//         expiresIn: "30s",
//       });

//       // const refreshToken = jwt.sign({ data: phone }, JWT_REFRESH_TOKEN, { expiresIn: '1y' });
//       // refreshTokens.push(refreshToken);
//       // console.log("refersh:::",refreshToken);
//       res
//         .status(202)
//         .cookie("accessToken", accessToken, {
//           expires: new Date(new Date().getTime() + 30 * 1000),
//           sameSite: "strict",
//           httpOnly: true,
//         })
//         // .cookie('refreshToken', refreshToken, {
//         // 	expires: new Date(new Date().getTime() + 31557600000),
//         // 	sameSite: 'strict',
//         // 	httpOnly: true
//         // })
//         .cookie("authSession", true, {
//           expires: new Date(new Date().getTime() + 30 * 1000),
//           sameSite: "strict",
//         })
//         .cookie("refreshTokenID", true, {
//           expires: new Date(new Date().getTime() + 31557600000),
//           sameSite: "strict",
//         })
//         .send({ msg: "Password Changed successfully" });
//     } else {
//       // console.log("not authenticated");
//       return res
//         .status(400)
//         .send({ verification: false, msg: "Incorrect OTP" });
//     }
//   })
// );

/**
 * Notification
 */

authRouter.post(
  "/image-upload",
  [
    // body("image_file")
    //   .exists()
    //   .withMessage("image_file must be exist")
    //   .bail()
    //   .notEmpty()
    //   .withMessage("image_file field is required")
    //   .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    console.log("body", req.body);

    return res.status(200).json(createJsonResponse(200, "sucsess", {}));
  })
);

/**
 * Social Login
 */
authRouter.post(
  "/social-login",
  [
    body("soicalId")
      .exists()
      .withMessage("soicalId must be exist")
      .bail()
      .notEmpty()
      .withMessage("soicalId field is required")
      .bail(),
    body("loginType")
      .exists()
      .withMessage("loginType must be exist")
      .bail()
      .notEmpty()
      .withMessage("loginType field is required")
      .bail(),
    body("deviceType")
      .exists()
      .withMessage("deviceType must be exist")
      .bail()
      .notEmpty()
      .withMessage("deviceType field is required")
      .bail(),
    body("deviceToken")
      .exists()
      .withMessage("deviceToken must be exist")
      .bail()
      .notEmpty()
      .withMessage("deviceToken field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, errors.array()[0].msg));
    }

    const { soicalId } = req.body;

    const [socialRows] = await User.getUserBySoicalId(soicalId);

    console.log(socialRows);

    if (socialRows.length === 0) {
      const [userRows] = await User.insertSocialUser(req.body);

      // console.log({ ...userRows }.insertId);
      const userid = { ...userRows }.insertId;
      const token = jwt.generateJWT({ id: userid });

      await User.updateUserToken({ token, id: userid });

      // console.log("USerId", userid);
      const [getmobile] = await User.getUserById(userid);

      // console.log("bytdfjksd", getmobile);
      const userData = getmobile[0];

      // console.log("userDta", userDa0ta);
      delete userData.password;
      delete userData.deviceToken;

      // delete userData.token;
      return res
        .status(200)
        .json(createJsonResponse("OK", 200, "Successfull", { ...userData }));
    }

    const socialResponse = socialRows[0];
    // console.log("SocialRespopnse", socialResponse);
    // console.log("REqBody", req.body);

    if (socialResponse.name !== "") {
      delete req.body.name;
    }

    if (socialResponse.email !== "") {
      delete req.body.email;
    }

    if (socialResponse.profileImage !== null) {
      delete req.body.profileImage;
    }

    await User.updateUserBySocialId(req.body);

    const [socialRowsData] = await User.getUserBySoicalId(soicalId);
    // console.log("SocialRdsfknjskd", socialRowsData[0]);

    const userData = socialRowsData[0];
    delete userData.password;
    delete userData.deviceToken;

    // delete userData.token;
    return res
      .status(200)
      .json(createJsonResponse("OK", 200, "Successfull", { ...userData }));
  })
);

module.exports = authRouter;
