const express = require("express");
const asyncHandler = require("express-async-handler");

const CMS = require("../../models/PrivacyPolicy.js");
const utils = require("../../../utils/utils.js");
const { createJsonResponse } = require("../../../utils/serverResponse.js");

const CMSRouter = express.Router();

/**
 * @route GET "/api/getAboutus/"
 */
CMSRouter.get(
  "/privacy-policy",
  asyncHandler(async (req, res) => {
    // Get all CMSs
    const [allCMSRows] = await CMS.getAllPrivacy();

    delete allCMSRows[0].updatedAt;

    const privacy = allCMSRows[0];

    delete privacy.id;
    delete privacy.createdAt;

    const name= privacy.name

    return res
      .status(200)
      .json(createJsonResponse("OK", 200, "Success", name));
  })
);

/**
 * @route GET "/api/getTerms/"
 */
CMSRouter.get(
  "/term-conditions",
  asyncHandler(async (req, res) => {
    // Get all CMSs
    const [allCMSRows] = await CMS.getAllTermCondition();
    // console.log("Alljhdfjvbj", allCMSRows);
    delete allCMSRows[0].updatedAt;
    const tnc = allCMSRows[0];

    delete tnc.id;
    delete tnc.createdAt;

    const name = tnc.name

    return res
      .status(200)
      .json(createJsonResponse("OK", 200, "Success", name));
  })
);

module.exports = CMSRouter;
