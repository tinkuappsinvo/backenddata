const express = require("express");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

const User = require("../../models/User.js");
const utils = require("../../../utils/utils.js");
const jwt = require("../../../utils/jwt.js");

const { createJsonResponse } = require("../../../utils/serverResponse.js");

const UserRouter = express.Router();

/**
 * @route GET "/api/User/"
 */
UserRouter.get(
  "/",
  asyncHandler(async (req, res) => {
    // Get all Users
    const [allUserRows] = await User.getAllUsers();

    return res.json(createJsonResponse(200, "Successfull", allUserRows));
  })
);

/**
 * @route GET "/api/User/:id"
 */
UserRouter.get(
  "/userDetails",
  asyncHandler(async (req, res) => {
    // Get User by id
    const usertoken = req.headers.authorization;
    const tokenData = usertoken.split(" ");
    if (tokenData) {
      const decoded = jwt.verifyJWT(tokenData[1], "secret-key");
      //   console.log(decoded.id);
      const userId = decoded.id;

      const [userRows] = await User.getUserById(userId);

      const user = userRows[0][0];

      delete user.token;
      const token = tokenData[1];

      return res
        .status(200)
        .json(createJsonResponse({ user, token }, "Successfull", true, 200));
    } else {
      return res
        .status(200)
        .json(createJsonResponse({}, "Successfull", false, 200));
    }
  })
);

/**
 * @route PUT "/api/User/editProfile"
 */
UserRouter.put(
  "/editProfile",
  asyncHandler(async (req, res) => {
    // console.log("Rq Body of token data::", req.body);
    const usertoken = req.headers.authorization;

    // console.log("authToken::", req.headers.authorization);
    const tokenData = usertoken.split(" ");

    // console.log("token", token);
    if (tokenData && req.body) {
      const decoded = jwt.verifyJWT(tokenData[1], "secret-key");
      const UserReqData = req.body;
      const userId = decoded.id;

      // console.log("bodyData",req.body)

      const name = req.body.full_name;
      const countryCode = req.body.country_code;
      const mobileNumber = req.body.mobile_number;
      const { image } = req.body;

      const Body = { name, countryCode, mobileNumber, image };

      const [rows] = await User.updateUserById(Body, userId);

      const [userRows] = await User.getUserById(userId);

      const user = userRows[0][0];

      delete user.token;
      const token = tokenData[1];

      return res
        .status(200)
        .json(createJsonResponse({ user, token }, "Successfull", true, 200));
    } else {
      return res
        .status(200)
        .json(createJsonResponse({}, "Successfull", false, 200));
    }
  })
);

/**
 * @route Post "/api/User/verify"
 */
//  UserRouter.post(
//     "/resetPasword",
//     asyncHandler(async (req, res) => {
//         const UserReqData = req.body;

//         const [rows] = await User.resetPassword(UserReqData);

//         return res.json(createJsonResponse(200, "Successfull", rows));
//     })
// );

/**
 * @route PUT "/api/User/updatePassword"
 */
UserRouter.post(
  "/resetPasword",
  [
    body("password")
      .exists()
      .withMessage("password must be exist")
      .bail()
      .notEmpty()
      .withMessage("password should not be empty")
      .bail(),
  ],

  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json(createJsonResponse(400, "Invalid json body", errors.errors[0]));
    }

    const { password } = req.body;

    await User.resetPassword({
      ...req.body,
      password: utils.encryptPassword(password),
    });
    // console.log("bodyjdsfjjd",req.body);
    return res.json(createJsonResponse(200, "Successfull", req.body));
  })
);

/**
 * @route DELETE "/api/User/:id"
 */
UserRouter.delete(
  "/:id",
  asyncHandler(async (req, res) => {
    const UserId = req.params.id;

    const [rows] = await User.deleteUserById(UserId);

    return res.json(createJsonResponse(200, "Successfull", rows));
  })
);

/**
 * @route Post "/api/User/distance"
 */
UserRouter.post(
  "/distance/:id",
  asyncHandler(async (req, res) => {
    // Get User Distance
    const result = await User.getDistance(req.body, req.params.id);
    // console.log("result::", result[0][0][0]);
    const distance = result[0][0][0].distance + " Km";
    delete result[0][0][0].id;

    return res.json(
      createJsonResponse(200, "Successfull", { ...result[0][0][0], distance })
    );
  })
);

/**
 * Add Category
 */

UserRouter.post(
  "/addCategory",
  [
    body("category_ids")
      .exists()
      .withMessage("category_ids must be exist")
      .bail()
      .notEmpty()
      .withMessage("The category_ids field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(200)
        .json(createJsonResponse({}, errors.array()[0].msg, false, 200));
    }
    const usertoken = req.headers.authorization;
    if (usertoken) {
      const token = usertoken.split(" ");

      const category_ids = req.body.category_ids;
      const categoryId = category_ids;
      // console.log("CategoryId", categoryId);
      // console.log("token", token);
      if (token) {
        const decoded = jwt.verifyJWT(token[1], "secret-key");
        const userId = decoded.id;
        // console.log(userId);

        // const [rows] = await Categories.addCategory(categoryId, userId);
        const [rows] = await SubCatagory.getSubCategoryByCategoryId(categoryId);

        const sub_categories = rows;

        // const [rows] = await Categories.addCategory(UserReqData,userId);

        return res.json(
          createJsonResponse({ sub_categories }, "Successfull", true, 200)
        );
      }
    } else {
      return res.json(createJsonResponse(400, "Id notfound", errors));
    }
  })
);

/**
 * Add subCategory
 */

UserRouter.post(
  "/addSubCategory",
  [
    body("subcategory_ids")
      .exists()
      .withMessage("subcategory_ids must be exist")
      .bail()
      .notEmpty()
      .withMessage("The subcategory_ids field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(200)
        .json(createJsonResponse({}, errors.array()[0].msg, false, 200));
    }

    const usertoken = req.headers.authorization;

    if (usertoken) {
      const token = usertoken.split(" ");

      const category_ids = req.body;
      const categoryId = category_ids;

      // console.log("token", token);
      if (token) {
        const decoded = jwt.verifyJWT(token[1], "secret-key");
        const userId = decoded.id;

        // const [rows] = await Categories.addCategory(categoryId, userId);

        // const [rows] = await Categories.addCategory(UserReqData,userId);

        return res.json(
          createJsonResponse({}, "sub-category added successfully.", true, 200)
        );
      }
    } else {
      return res.json(
        createJsonResponse(
          {},
          "Session expired, please login again.",
          false,
          200
        )
      );
    }
  })
);

/**
 * Add Intrest
 */

UserRouter.post(
  "/addInterest",
  [
    body("interest_ids")
      .exists()
      .withMessage("interest_ids must be exist")
      .bail()
      .notEmpty()
      .withMessage("The interest_ids field is required")
      .bail(),
  ],
  asyncHandler(async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(200)
        .json(createJsonResponse({}, errors.array()[0].msg, false, 200));
    }

    const usertoken = req.headers.authorization;

    if (usertoken) {
      const token = usertoken.split(" ");

      const category_ids = req.body;
      const categoryId = category_ids;

      // console.log("token", token);
      if (token) {
        const decoded = jwt.verifyJWT(token[1], "secret-key");
        const userId = decoded.id;

        // const [rows] = await Categories.addCategory(categoryId, userId);

        const [rows] = await Categories.getCategory();

        const categories = rows;

        // const [rows] = await Categories.addCategory(UserReqData,userId);

        return res.json(
          createJsonResponse(
            { categories },
            "Interests added successfully.",
            true,
            200
          )
        );
      }
    } else {
      return res.json(
        createJsonResponse(
          {},
          "Session expired, please login again.",
          false,
          200
        )
      );
    }
  })
);

UserRouter.put(
  "/updateDeviceToken",
  asyncHandler(async (req, res) => {
    const usertoken = req.headers.authorization;
    const token = usertoken.split(" ");

    if (token) {
      const decoded = jwt.verifyJWT(token[1], "secret-key");
      const UserReqData = req.body;
      const userId = decoded.id;

      return res.json(createJsonResponse(200, "Successfull"));
    } else {
      return res.json(createJsonResponse(400, "Id notfound", errors));
    }
  })
);

module.exports = UserRouter;
