const express = require("express");
const asyncHandler = require("express-async-handler");
const { body, validationResult } = require("express-validator");

const User = require("../../models/User.js");
const utils = require("../../../utils/utils.js");
const jwt = require("../../../utils/jwt.js");

const { createJsonResponse } = require("../../../utils/serverResponse.js");

const FileRouter = express.Router();

FileRouter.get(
  "/upload",
  asyncHandler(async (req, res) => {
      
    if (req.files) {
        console.log(req.files);
        const file = req.files.file;
        const fileName = file.name;
        console.log("FileName", fileName);
    
        file.mv("./public/" + fileName, (err) => {
          if (err) {
            res.send(err);
          } else {
            res.send("File Uploaded");
          }
        });
      }
  })
);

module.exports = FileRouter;
