const { connect } = require("../../database/db.config.js");
const { currentDateTime } = require("../../utils/date.js");

const User = {};

User.getAllUsers = async () => {
  const result = await connect.query(
    "select id, name, email, createdAt from user"
  );
  return result;
};

User.getUserById = async (id) => {
  const result = await connect.query(`select * from user where id=?`, [id]);
  return result;
};

User.getUserByEmail = async (email) => {
  const result = await connect.query(`select * from user where email=?`, [
    email,
  ]);
  return result;
};

User.getUserByMobile = async (contactNumber) => {
  const result = await connect.query(
    "select * from user where contactNumber=?",
    [contactNumber]
  );
  return result;
};

User.getUserBySoicalId = async (soicalId) => {
  const result = await connect.query("select * from user where soicalId=?", [
    soicalId,
  ]);
  return result;
};

User.insertSocialUser = async (body) => {
  // insert into user set name=?,email=?,password=?,countryCode=?,contactNumber=?,anniversary=?,gender=?
  //   dob=?,profileImage=?,deviceType=?,isVerify=?,createdAt=?
  const result = await connect.query(
    `insert into user set name=?, email=?,profileImage=?,isVerify=1,soicalId=?,loginType=?,deviceType=?,deviceToken=?,createdAt=?`,
    [
      body.name,
      body.email,
      body.profileImage,
      body.soicalId,
      body.loginType,
      body.deviceType,
      body.deviceToken,
      currentDateTime(),
    ]
  );

  return result;
};

User.insertUser = async (body) => {
  console.log("Body", body);
  const isVerify = 0;
  console.log(currentDateTime());
  // insert into user set name=?,email=?,password=?,countryCode=?,contactNumber=?,anniversary=?,gender=?
  //   dob=?,profileImage=?,deviceType=?,isVerify=?,createdAt=?
  const result = await connect.query(`call insertUser(?,?,?,?,?,?,?,?,?,?);`, [
    body.name,
    body.email,
    body.password,
    body.countryCode,
    body.contactNumber,
    body.anniversary,
    body.gender,
    body.dob,
    body.profileImage,
    body.deviceType,
  ]);

  return result;
};

User.UserVerify = async (id) => {
  const result = await connect.query(
    `update user set isVerify = 1 , updatedAt=? where id=?`,
    [currentDateTime(), id]
  );
  return result[0];
};

User.forgotPassword = async (body) => {
  const result = await connect.query(
    `update user set password=?, updatedAt=? where email=?`,
    [body.password, currentDateTime(), body.email]
  );

  return result;
};

User.resetPassword = async (body, userId) => {
  console.log("Bobfgf", body.password);
  const result = await connect.query(
    `update user set password=?, updatedAt=? where id=?`,
    [body.password, currentDateTime(), body.userId]
  );

  return result;
};

User.updateUserById = async (body, id) => {
  // console.log("fdghdfj", body);
  const { ...restBody } = body;

  delete restBody.password;
  delete restBody.email;

  const keys = Object.keys(restBody);

  let queryString = "";
  const valueArray = [];

  keys.forEach((key) => {
    queryString += `${key}=?,`;
    valueArray.push(restBody[key]);
  });

  const result = await connect.query(
    `update user set ${queryString} updatedAt=? where id=?`,
    [...valueArray, currentDateTime(), id]
  );

  return result;

  // const result = await connect.query(
  //   `update user set name=?,countryCode=?, mobileNumber=?, image=?, updatedAt=? where id=?`,
  //   [
  //     body.full_name,
  //     body.country_code,
  //     body.mobile_number,
  //     body.image,
  //     currentDateTime(),
  //     id,
  //   ]
  // );

  // return result;
};

User.updateUserByEmail = async (body) => {
  const result = await connect.query(
    `update user set social_id=?, deviceType=?,socialType=?,updatedAt=? where email=?`,
    [
      body.social_id,
      body.deviceType,
      body.socialType,
      currentDateTime(),
      body.email,
    ]
  );

  return result;
};

User.updateUserPassword = async (body) => {
  const { id, ...restBody } = body;

  delete restBody.email;
  delete restBody.name;
  delete restBody.createdDate;
  delete restBody.verify;

  const keys = Object.keys(restBody);

  let queryString = "";
  const valueArray = [];

  keys.forEach((key) => {
    queryString += `${key}=?,`;
    valueArray.push(restBody[key]);
  });

  const result = await connect.query(
    `update user set ${queryString} updatedAt=? where id=?`,
    [...valueArray, currentDateTime(), id]
  );

  return result;
};

User.takePasswordById = async (userId) => {
  const result = await connect.query(`select password from user where id=?`, [
    userId,
  ]);

  return result;
};

// User.updateDeviceToken = async (id) => {
//   const result = await connect.query(
//     "update user set _token = ? where id=?",
//     [id]
//   );
//   return result;
// };

User.updateUserToken = async (body) => {
  // console.log("djgk",body)
  const result = await connect.query("update user set token = ? where id=?", [
    body.token,
    body.id,
  ]);
  return result;
};

User.updateUserImage = async (body) => {
  // console.log("djgk",body)
  const result = await connect.query(
    "update user set profileImage = ? where id=?",
    [body.fileName, body.userId]
  );
  return result;
};
// User.updateUserBySocialId = async (body) => {
//   // console.log("djgk",body)
//   const result = await connect.query("update user set token = ? where =?", [
//     body.token,
//     body.id,
//   ]);
//   return result;
// };

User.updateUserBySocialId = async (body) => {
  console.log("Bodsgfhjghjsd", body);
  const { soicalId, ...restBody } = body;

  console.log("REstBody", restBody);
  console.log("SocialId", soicalId);

  const keys = Object.keys(restBody);

  let queryString = "";
  const valueArray = [];

  keys.forEach((key) => {
    queryString += `${key}=?,`;
    valueArray.push(restBody[key]);
  });

  const result = await connect.query(
    `update user set ${queryString} updatedAt=? where soicalId=?`,
    [...valueArray, currentDateTime(), soicalId]
  );

  return result;
};

module.exports = User;
