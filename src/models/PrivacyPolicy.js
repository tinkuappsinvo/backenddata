const { connect } = require("../../database/db.config.js");
const { currentDateTime } = require("../../utils/date.js");

const Privacy = {};

Privacy.getAllPrivacy = async () => {
  const result = await connect.query("select * from privacyPolicy");
  return result;
};

Privacy.getAllTermCondition = async () => {
  const result = await connect.query("select * from tandc");
  return result;
};

module.exports = Privacy;
