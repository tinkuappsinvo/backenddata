const express = require("express");

const authRouter = require("../controllers/Auth/Auth.js");
const userRouter = require("../controllers/User/User.js");
const CMSRouter = require("../controllers/PrivacyAndTerms/PrivacyAndTerms.js");
const FileRouter = require("../controllers/FileUpload/FileUpload.js");

const router = express.Router();

router.use("/api/auth", authRouter);

router.use("/", authRouter);
router.use("/saloon", FileRouter);

router.use("/api/auth", userRouter);

router.use("/api", CMSRouter);


module.exports = router;
